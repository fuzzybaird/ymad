<?php namespace Flynsarmy\Redirection\Controllers;

use BackendMenu;
use Request;
use Validator;
use Flash;
use DB;
use Backend\Classes\Controller;
use Flynsarmy\Redirection\Models\Redirect;
use Flynsarmy\Redirection\Classes\Manager;
use System\Classes\ApplicationException;
use System\Classes\SettingsManager;

class Redirects extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.ImportExportController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $importExportConfig = 'config_import_export.yaml';

    public $requiredPermissions = ['flynsarmy.redirection.access_redirects'];

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('October.System', 'system', 'settings');
        SettingsManager::setContext('Flynsarmy.Redirection', 'redirects');
        $this->addCss('/plugins/flynsarmy/redirection/assets/css/redirects.css');
    }

    public function listInjectRowClass($record, $definition = null)
    {
        if ( !$record->enabled )
            return 'disabled';
    }

    /**
     * Deleted checked users.
     */
    public function index_onDelete()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            $redirect = new Redirect;
            DB::table($redirect->getTable())->whereIn('id', $checkedIds)->delete();
            $redirect->flushCache();

            Flash::success('Successfully deleted the selected redirects.');
        }
        else {
            Flash::error('There are no selected redirects to delete.');
        }

        return $this->listRefresh();
    }

    /**
     * Disable checked users.
     */
    public function index_onDisable()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            $redirect = new Redirect;
            DB::table($redirect->getTable())->whereIn('id', $checkedIds)->update(['enabled' => 0]);
            $redirect->flushCache();

            Flash::success('Successfully disabled the selected redirects.');
        }
        else {
            Flash::error('There are no selected redirects to disable.');
        }

        return $this->listRefresh();
    }

    /**
     * Enable checked users.
     */
    public function index_onEnable()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            $redirect = new Redirect;
            DB::table($redirect->getTable())->whereIn('id', $checkedIds)->update(['enabled' => 1]);
            $redirect->flushCache();

            Flash::success('Successfully enabled the selected redirects.');
        }
        else {
            Flash::error('There are no selected redirects to enable.');
        }

        return $this->listRefresh();
    }

    public function index_onTestURL()
    {
        $validator = Validator::make(
            Request::all(),
            ['url' => ['required', 'url']]
        );

        if ( $validator->fails() )
            throw new ApplicationException($validator->messages()->first());

        //$matches = Redirect::triggeredByURL(Request::input('url'))->get();
        $matches = Manager::instance()->findMatches(Request::input('url'));

        return [
            '#testURLResults' => $this->makePartial('test_url_results', [
                'matches' => $matches,
            ])
        ];
    }

    public function index_onFlushCache()
    {
        Manager::instance()->flushAll();
        Flash::success('Cache flushed successfully.');
    }

    public function reorder()
    {
        $this->pageTitle = 'Reorder Redirects';

        $this->addCss('/plugins/flynsarmy/redirection/assets/vendor/flynsarmy/tabletreelist/css/tabletreelist.css');
        $this->addJs('/plugins/flynsarmy/redirection/assets/vendor/flynsarmy/tabletreelist/js/tabletreelist.js');

        $toolbarConfig = $this->makeConfig();
        $toolbarConfig->buttons = '$/flynsarmy/redirection/controllers/redirects/_reorder_toolbar.htm';

        $this->vars['toolbar'] = $this->makeWidget('Backend\Widgets\Toolbar', $toolbarConfig);
        $this->vars['records'] = Redirect::reorderColumns()->sorted()->get();
    }

    public function reorder_onMove()
    {
        $itemId = post('sourceNode', 0);
        $itemIds = post('sortedIds', []);

        $sourceNode = Redirect::find($itemId);

        if ( !$sourceNode || !$itemIds )
            return;

        $itemOrders = array_map(function($val) {
            return ++$val;
        }, array_keys($itemIds));

        $sourceNode->setSortableOrder($itemIds, $itemOrders);
    }
}