<?php namespace Flynsarmy\Redirection\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateRedirectsTable extends Migration
{

    public function up()
    {
        Schema::create('flynsarmy_redirection_redirects', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->boolean('enabled')->default(false)->index();
            $table->string('match_type')->default('');
            $table->string('from_url')->default('');
            $table->string('to_url')->default('');
            $table->integer('redirect_code')->default(301);
            $table->integer('hits')->default(0)->unsigned();
            $table->integer('sort_order')->default(0)->unsigned()->index();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('flynsarmy_redirection_redirects');
    }

}
