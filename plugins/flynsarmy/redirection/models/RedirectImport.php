<?php namespace Flynsarmy\Redirection\Models;

use Exception;
use Backend\Models\ImportModel;
use Flynsarmy\Redirection\Models\Redirect;

class RedirectImport extends ImportModel
{
    /**
     * @var array The rules to be applied to the data.
     */
    public $rules = [];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        // Set up required fields and other validation rules
        $this->rules = (new Redirect)->rules;
    }

    public function importData($results, $sessionKey = null)
    {
        foreach ($results as $row => $data)
        {
            try {
                $redirect = new Redirect;
                $redirect->fill($data);
                $redirect->save();

                $this->logCreated();
            }
            catch (Exception $ex) {
                $this->logError($row, $ex->getMessage(). ' ' . print_r($data, true));
            }
        }
    }
}