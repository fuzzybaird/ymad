<?php namespace Flynsarmy\Redirection\Models;

use DB;
use DbDongle;
use Model;
use Backend;
use Flynsarmy\Redirection\Classes\Manager;
use October\Rain\Support\ValidationException;

class Redirect extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Sortable;

    const MATCH_EXACT = 'Matches Exactly';
    const MATCH_STARTS_WITH = 'Starts With';
    const MATCH_ENDS_WITH = 'Ends With';
    const MATCH_REGEX = 'Regex';

    public $table = 'flynsarmy_redirection_redirects';

    // public $implement = ['October.Rain.Database.Behaviors.SortableModel'];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /*
     * Validation
     */
    public $rules = [
        'from_url' => ['required'],
        'to_url' => ['required'],
        'match_type' => ['required'],
        'redirect_code' => ['required'],
        'hits' => ['integer'],
    ];

    /**
     * Determine where we're redirecting to based on the given URL
     *
     * @param  string $url The URL we've hit
     *
     * @return string      The URL to redirect to
     */
    public function toUrl( $url )
    {
        if ( $this->match_type != 'Regex' )
            return $this->to_url;
        else
            return preg_replace(
                "!".str_replace(array('!', '?'), array('\!', '\?'), $this->from_url)."!",
                $this->to_url,
                $url
            );
    }

    public function scopeEnabled($query)
    {
        return $query->where('enabled', '=', 1);
    }

    public function scopeTriggeredByURL($query, $url)
    {
        // Multiple instances of the same named binding aren't working, so
        // sent $url multiple times. Gah.
        return $query->whereRaw("
            CASE match_type
                WHEN '{self::MATCH_STARTS_WITH}' THEN ? LIKE CONCAT(from_url,'%')
                WHEN '{self::MATCH_ENDS_WITH}' THEN ? LIKE CONCAT('%', from_url)
                WHEN '{self::MATCH_REGEX}' THEN ? REGEXP REPLACE(from_url, '(\?:', '(')
                ELSE ? = from_url
            END
        ", [$url, $url, $url, $url]);
    }

    public function scopeSorted($query)
    {
        return $query->orderBy('sort_order');
    }

    public function afterValidate()
    {
        // Don't allow admin from URLs or users may lock themself out of admin
        $backendUrl = Backend::url();
        if ( strpos($this->from_url, $backendUrl) !== false )
            throw new ValidationException([
                'from_url' => 'From URL must not be an admin URL.'
            ]);
    }

    public function scopeReorderColumns($query)
    {
        return $query->select(
            'id', 'from_url', 'to_url',
            DbDongle::raw("CASE match_type WHEN '{self::MATCH_EXACT}' THEN 'Exact Match' ELSE match_type END AS match_type"),
            DbDongle::raw("CASE redirect_code WHEN 301 THEN 'Permanent' WHEN 302 THEN 'Temporary' ELSE 'Unknown' END AS redirect_code"),
            'hits'
        );
    }

    public function getMatchTypeOptions()
    {
        $types = [];
        $types[self::MATCH_EXACT] = 'Matches Exactly';
        $types[self::MATCH_STARTS_WITH] = 'Starts With';
        $types[self::MATCH_ENDS_WITH] = 'Ends With';
        $types[self::MATCH_REGEX] = 'Regex';

        return $types;
    }

    /*
     * Delete all cache every time a redirect is added/updated.
     */
    public function beforeSave()
    {
        $this->flushCache();
    }

    /*
     * Delete all cache every time a redirect is deleted.
     */
    public function beforeDelete()
    {
        $this->flushCache();
    }

    /**
     * Flush all cache
     *
     * @throws \League\Flysystem\Exception
     */
    public function flushCache()
    {
        $manager = Manager::instance();

        // We can only flush all cache if we're using a decent Cache driver
        if ( $manager->canCacheWithTags() )
            $manager->flushAll();
    }
}