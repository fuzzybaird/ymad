<?php namespace Flynsarmy\Redirection\Models;

use Backend\Models\ExportModel;
use Flynsarmy\Redirection\Models\Redirect;

class Redirectexport extends ExportModel
{
    public function exportData($columns, $sessionKey = null)
    {
        $redirects = Redirect::all();
        $redirects->each(function($subscriber) use ($columns) {
            $subscriber->addVisible($columns);
        });
        return $redirects->toArray();
    }
}