<?php namespace Flynsarmy\Redirection;

use Backend;
use DB;
use Event;
use URL;
use Cms\Classes\Controller;
use Flynsarmy\Redirection\Classes\Manager;
use Flynsarmy\Redirection\Models\Redirect;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{

    public function pluginDetails()
    {
        return [
            'name'        => 'Redirection',
            'description' => 'Provides a GUI interface for adding site redirects',
            'author'      => 'Flyn San',
            'icon'        => 'icon-pencil'
        ];
    }

    public function registerSettings()
    {
        return [
            'redirects' => [
                'label'       => 'Redirection',
                'icon'        => 'icon-random',
                'description' => 'Configure site redirects.',
                'url'         => Backend::url('flynsarmy/redirection/redirects'),
                'order'       => 210,
                'permissions' => ['flynsarmy.redirection.access_redirects'],
            ]
        ];
    }

    public function registerPermissions()
    {
        return [
            'flynsarmy.redirection.access_redirects'          => ['label' => 'Redirection - Access Redirects', 'tab' => 'Flynsarmy'],
        ];
    }

    public function boot()
    {
        Event::listen('cms.page.beforeDisplay', function(Controller $controller, $url, $page) {
            // Convert to absolute URL
            $url = URL::full($url);
            $manager = Manager::instance();

            $match = $manager->match($url);
            if ( $match )
            {
                // Update hit counter
                $redirect = new Redirect;
                DB::table($redirect->getTable())
                    ->where('id', $match->id)
                    ->update(['hits' => DB::raw('hits + 1')]);

                // Do the redirect
                header("Location: ".$match->to_url, true, $match->redirect_code);
                exit;
            }


            //// Find an active match
            //$match = Redirect::enabled()->sorted()->triggeredByURL($url)->first();
            //
            //if ( $match )
            //{
            //    // Update hit counter
            //    $match->hits++;
            //    $match->save();
            //
            //    // Do the redirect
            //    header("Location: ".$match->toUrl($url), true, $match->redirect_code);
            //    exit;
            //}
        });
    }
}