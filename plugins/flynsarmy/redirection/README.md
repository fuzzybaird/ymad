# Redirection

This plugin adds redirect features to October CMS through a convenient GUI located in the settings area.

## Installation

* `git clone` to */plugins/flynsarmy/redirection*
* Log out and back in.