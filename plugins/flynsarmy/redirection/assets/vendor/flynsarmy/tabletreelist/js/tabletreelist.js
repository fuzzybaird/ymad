// http://johnny.github.io/jquery-sortable/

/*
 * TreeList Widget
 *
 * Supported options:
 *	- handle - class name to use as a handle
 *
 * Events:
 * - move.oc.treelist - triggered when a node on the tree is moved.
 *
 * Dependences:
 * - Sortable Plugin (october.sortable.js)
 */
+function ($) { "use strict";

	var TableTreeListWidget = function (element, options) {

		var $el = this.$el = $(element),
			self = this;

		this.options = options || {};

		$el.find('> table').sortable($.extend({}, {
			onDrop: function(item, container, _super, event) {
				var field,
				newIndex = item.index()

				if (newIndex != oldIndex)
					item.closest('table').find('tbody tr').each(function (i, row) {
						row = $(row)
						field = row.children().eq(oldIndex)
						if(newIndex)
							field.before(row.children()[newIndex])
						else
							row.prepend(field)
					})

				self.$el.trigger('move.oc.tabletreelist', { item: item, container: container })
				_super(item, container, _super, event)
			}
		}, this.options));
	}

	var oldIndex;

	TableTreeListWidget.DEFAULTS = {
		// handle: null,
		containerSelector: 'table',
		itemPath: '> tbody',
		itemSelector: 'tr',
		placeholder: '<tr class="placeholder"/>'
	}

	// TREELIST WIDGET PLUGIN DEFINITION
	// ============================

	var old = $.fn.tableTreeListWidget

	$.fn.tableTreeListWidget = function (option) {
		var args = arguments,
			result

		this.each(function () {
			var $this	 = $(this)
			var data		= $this.data('oc.treelist')
			var options = $.extend({}, TableTreeListWidget.DEFAULTS, $this.data(), typeof option == 'object' && option)
			if (!data) $this.data('oc.treelist', (data = new TableTreeListWidget(this, options)))
			if (typeof option == 'string') result = data[option].call($this)
			if (typeof result != 'undefined') return false
		})

		return result ? result : this
	}

	$.fn.tableTreeListWidget.Constructor = TableTreeListWidget

	// TREELIST WIDGET NO CONFLICT
	// =================

	$.fn.tableTreeListWidget.noConflict = function () {
		$.fn.treeListWidget = old
		return this
	}

	// TREELIST WIDGET DATA-API
	// ==============

	$(document).render(function(){
		$('[data-control="tabletreelist"]').tableTreeListWidget();
	})

}(window.jQuery);