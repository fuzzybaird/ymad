<?php namespace Flynsarmy\Redirection\Classes;

use Flynsarmy\Redirection\Models\Redirect;
use League\Flysystem\Exception;
use stdClass;
use Cache;
use DB;
use Config;

class Manager
{
    protected $cacheWithTags = true;
    protected $cacheMinutes = 1; // Only used if we don't support cache tags

    protected $cacheTag = 'flynsarmy.redirection';

    use \October\Rain\Support\Traits\Singleton;

    protected function init()
    {
        // File and Database caches don't support tags. So check for them.
        $driver = Config::get('cache.default');
        if ( $driver == 'database' || $driver == 'file' )
        {
            $this->cacheWithTags = false;
            $this->cacheMinutes = Config::get('cache.redirectionCacheMinutes', 1);
        }
    }

    public function canCacheWithTags()
    {
        return $this->cacheWithTags;
    }

    public function flush($url)
    {
        Cache::forget($url);
    }

    public function flushAll()
    {
        if ( !$this->canCacheWithTags() )
            throw new Exception("You are not using a cache driver that supports this.");

        Cache::tags($this->cacheTag)->flush();
    }

    /**
     * Find a redirect for a given URL (cached).
     *
     * @param $url             URL to find match for.
     * @return stdClass|false  The DB record matching this URL
     */
    public function match($url)
    {
        $generator = function() use (&$url) {
            $matches = $this->findMatches($url);

            if ( sizeof($matches) )
            {
                $matches[0]->to_url = $this->getToUrl($url, reset($matches));
                return $matches[0];
            }

            return false;
        };

        if ( $this->canCacheWithTags() )
            return Cache::tags($this->cacheTag)->rememberForever($url, $generator);
        else
            return Cache::remember($url, $this->cacheMinutes, $generator);
    }

    /**
     * Find all matches for the given URL (uncached).
     *
     * @param $url
     * @return array of stdClass records
     */
    public function findMatches($url)
    {
        $matches = [];
        $records = Redirect::enabled()->sorted()->getQuery()
            ->get(['id', 'match_type', 'from_url', 'to_url', 'redirect_code']);

        foreach ( $records as $record )
            if ( $this->willTrigger($url, $record) )
                $matches[] = $record;

        return $matches;
    }

    /**
     * Determines if a URL will trigger a given record match.
     *
     * @param $url
     * @param stdClass $match
     * @return bool
     */
    public function willTrigger($url, stdClass $match)
    {
        switch ( $match->match_type )
        {
            case REDIRECT::MATCH_EXACT:
                return $url === $match->from_url;

            case REDIRECT::MATCH_STARTS_WITH:
                return strpos($url, $match->from_url) === 0;

            case REDIRECT::MATCH_ENDS_WITH:
                return $match->from_url === substr($url, -strlen($match->from_url));

            case REDIRECT::MATCH_REGEX:
                return !!preg_match('!'.$match->from_url.'!', $url);
        }

        return false;
    }

    /**
     * Convert a match to a URL to redirect to.
     *
     * @param $url
     * @param stdClass $match
     * @return string
     */
    public function getToUrl($url, stdClass $match)
    {
        if ( $match->match_type == Redirect::MATCH_REGEX )
        {
            return preg_replace('!'.$match->from_url.'!', $match->to_url, $url);
        }
        else
            return $match->to_url;
    }
}