<?php namespace Fuzzy\Extension;
use raveren\kint;
use Cms\Classes\Controller;
use System\Classes\PluginBase;

/**
 * extension Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'extension',
            'description' => 'No description provided yet...',
            'author'      => 'fuzzy',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'Fuzzy\Extension\Components\Post' => 'Post',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'fuzzy.extension.some_permission' => [
                'tab' => 'extension',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'extension' => [
                'label'       => 'extension',
                'url'         => Backend::url('fuzzy/extension/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['fuzzy.extension.*'],
                'order'       => 500,
            ],
        ];
    }

}
